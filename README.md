# cspy3

Create Serial Port Packet Processor (CSP3) for the iRobot Create.



## Features

- Control the Create!
- Request sensor information!
- Parse packets!

## Install for development

```bash
git clone https://gitlab.com/rldotai/cspy3.git
cd cspy3
pip install --editable .
```

The `pyserial` package is needed for `cspy3`, but since its move to version 3.0 the API has changed slightly.
Issues with the `serial` module may be fixed in the latest version, which can be installed via:

```bash
pip install git+https://github.com/pyserial/pyserial
```


# TODO

- [x] Example of basic behavior using threads
- [x] Implement thread-safe parser w/ deque
- [x] Test robot control using threads
- [ ] Profile and evaluate performance
- [ ] Implement worst-case acting loop changing.
- [x] Ensure safe closing of serial port when program exits
- [ ] Implement the remaining commands from Open Interface
- [ ] Log run information to file

## Improvements and Alternatives

- [ ] Simplify project structure 
    + Remove unused code
    + Consolidate contants in `create_v1.py`
- [ ] Implement simple command line demo w/ `argparse`
- [ ] Try using `multiprocessing` or `asyncio` instead of `threading`
- [ ] Implement packet parser as a generator
- [ ] Add reflex code for dangerous situations or when no new data has been received for a certain period of time.
- [ ] Compatibility for Create V2 and iRobot Roombas.
- [ ] Incorporate webcam video feed
- [ ] Standardize logging

## Contributors

- Brendan Bennett (rldotai)
- Dylan Ashley (dylan-ashley)

## License

MIT. See the [LICENSE](LICENSE) file for more details.
