"""
cspy3 - Create Serial Port Packet Processor (CSP3) for the iRobot Create.
"""

__version__ = '0.0.1'
__author__ = 'rldotai <babennet@ualberta.ca>'


# Set up logging
import logging
import sys
_log_format = '%(asctime)s %(name)-12s %(levelname)-4s %(message)s'
if __debug__:
    logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=_log_format)
else:
    logging.basicConfig(format=_log_format, stream=sys.stderr)




from . import create_v1 as v1
from .controller import Controller 
from .packet_parser import Parser

# don't export modules unless they're in the whitelist
import inspect
_whitelist = []


# define `__all__` for this package
__all__ = [name for name, x in locals().items() if not name.startswith('_') and
           (not inspect.ismodule(x) or x in _whitelist)]
